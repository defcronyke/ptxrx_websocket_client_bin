use futures_util::stream::FuturesUnordered;
use futures_util::StreamExt;
use std::time::Instant;
// we will use tungstenite for websocket client impl (same library as what axum is using)
use ::ptxrx_util;
use ::ptxrx_websocket_client;

#[tokio::main]
async fn main() {
    let start_time = Instant::now();
    //spawn several clients that will concurrently talk to the server
    let mut clients = (0..ptxrx_util::conf::N_CLIENTS)
        .map(|cli| tokio::spawn(ptxrx_websocket_client::spawn_client(cli)))
        .collect::<FuturesUnordered<_>>();

    //wait for all our clients to exit
    while clients.next().await.is_some() {}

    let end_time = Instant::now();

    //total time should be the same no matter how many clients we spawn
    println!(
        "Total time taken {:#?} with {} concurrent clients, should be about 6.45 seconds.",
        end_time - start_time, ptxrx_util::conf::N_CLIENTS
    );
}